﻿using NeamaAPI.BL.Interface;
using NeamaAPI.BL.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using NeamaAPI.Models;
using NeamaAPI.Models.Outputs;
using NeamaAPI.Models.Inputs;
using NeamaAPI.API.Filters;
using System.Web.Http.Controllers;

namespace NeamaAPI.API.Controllers
{
    [auth]
    public class NeamaAPIController : ApiController
    {
        INeamaAPI neama = new NeamaAPI.BL.Implementation.NeamaAPI();

        [HttpPost]
        public DTO<GetRolesDTO> GetRoles(Input<Role> obj)
        {
            return neama.GetRoles(obj);
        }
        [HttpPost]
        public DTO<GetTechniciansDTO> GetTechnicians(Input<GetTechnicians> obj)
        {
            return neama.GetTechnicians(obj);
        }
        [HttpPost]
        public DTO<LoginDTO> Login(Input<Login> obj)
        {
            return neama.Login(obj);
        }
        [HttpPost]
        public DTO<GetWOBySupervisorDTO> GetWOBySupervisor(Input<GetWOBySupervisor> obj)
        {
            return neama.GetWOBySupervisor(obj);
        }
        [HttpPost]
        public DTO<SaveReportTechnicianDTO> SaveReportTechnician(Input<SaveReportTechnician> obj)
        {
            return neama.SaveReportTechnician(obj);
        }
        [HttpPost]
        public DTO<GetTrackTypeDTO> GetTrackType(Input<GetTrackType> obj)
        {
            return neama.GetTrackType(obj);
        }
        [HttpPost]
        public DTO<TrackDailyLogDTO> TrackDailyLog(Input<TrackDailyLog> obj)
        {
            return neama.TrackDailyLog(obj);
        }
        [HttpPost]
        public DTO<CloseWODTO> CloseWorkOrder(Input<CloseWO> obj)
        {
            return neama.CloseWorkOrder(obj);
        }
        [HttpPost]
        public DTO<CloseWODetailsDTO> CloseWorkOrderDetails(Input<CloseWODetails> obj)
        {
            return neama.CloseWorkOrderDetails(obj);
        }
        [HttpPost]
        public DTO<AddDeviceIDDTO> AddDeviceID(Input<AddDeviceID> obj)
        {
            return neama.AddDeviceID(obj);
        }

        #region task

        [HttpPost]
        public DTO<GetTaskTypesDTO> GetTaskTypes(Input<GetTaskTypes> obj)
        {
            return neama.GetTaskTypes(obj);
        }
        [HttpPost]
        public DTO<GetTaskStatusDTO> GetTaskStatus(Input<GetTaskStatus> obj)
        {
            return neama.GetTaskStatus(obj);
        }
        [HttpPost]
        public DTO<GetSalesTaskByUserIDDTO> GetSalesTaskByUserID(Input<GetSalesTaskByUserID> obj)
        {
            return neama.GetSalesTaskByUserID(obj);
        }
        [HttpPost]
        public DTO<GetMandoopTaskByUserIDDTO> GetMandoopTaskByUserID(Input<GetMandoopTaskByUserID> obj)
        {
            return neama.GetMandoopTaskByUserID(obj);
        }
        [HttpPost]
        public DTO<UpdateTaskSalesDTO> UpdateTaskSales(Input<UpdateTaskSales> obj)
        {
            return neama.UpdateTaskSales(obj);
        }
        [HttpPost]
        public DTO<UpdateTaskMandoopDTO> UpdateTaskMandoop(Input<UpdateTaskMandoop> obj)
        {
            return neama.UpdateTaskMandoop(obj);
        }
        [HttpPost]
        public DTO<CreateTaskSalesDTO> CreateTaskSales(Input<CreateTaskSales> obj)
        {
            return neama.CreateTaskSales(obj);
        }

        [HttpPost]
        public DTO<CreateTaskMandoopDTO> CreateTaskMandoop(Input<CreateTaskMandoop> obj)
        {
            return neama.CreateTaskMandoop(obj);
        }
        [HttpPost]
        public DTO<GetSearchTaskSalesDTO> GetSearchTaskSales(Input<GetSearchTaskSales> obj)
        {
            return neama.GetSearchTaskSales(obj);
        }
        [HttpPost]
        public DTO<GetSearchTaskMandoopDTO> GetSearchTaskMandoop(Input<GetSearchTaskMandoop> obj)
        {
            return neama.GetSearchTaskMandoop(obj);
        }
        #endregion
    }



    public class auth : BasicAuthenticationFilter
    {

        public auth()
        { }

        public auth(bool active)
            : base(active)
        { }


        public bool Authenticate(string username, string password)
        {
            if (username == "neama@ppuser" && password == "neama@pp321")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            if (username == "neama@ppuser" && password == "neama@pp321")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}