//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NeamaAPI.DAL
{
    using System;
    
    public partial class sp_Get_TankTypes_Result
    {
        public int ID { get; set; }
        public Nullable<int> ProcessStationID { get; set; }
        public string ProductName { get; set; }
        public string Gallon { get; set; }
        public string MachineNumber { get; set; }
        public string Picture { get; set; }
        public bool Status { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ProcessStationName { get; set; }
    }
}
