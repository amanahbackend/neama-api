﻿using NeamaAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.BL.Interface
{
    public interface INeamaAPI
    {
        DTO<Models.Outputs.GetRolesDTO> GetRoles(Input<Models.Inputs.Role> obj);
        DTO<Models.Outputs.GetTechniciansDTO> GetTechnicians(Input<Models.Inputs.GetTechnicians> obj);
        DTO<Models.Outputs.LoginDTO> Login(Input<Models.Inputs.Login> obj);
        DTO<Models.Outputs.GetWOBySupervisorDTO> GetWOBySupervisor(Input<Models.Inputs.GetWOBySupervisor> obj);
        DTO<Models.Outputs.SaveReportTechnicianDTO> SaveReportTechnician(Input<Models.Inputs.SaveReportTechnician> obj);
        DTO<Models.Outputs.GetTrackTypeDTO> GetTrackType(Input<Models.Inputs.GetTrackType> obj);
        DTO<Models.Outputs.TrackDailyLogDTO> TrackDailyLog(Input<Models.Inputs.TrackDailyLog> obj);
        DTO<Models.Outputs.CloseWODTO> CloseWorkOrder(Input<Models.Inputs.CloseWO> obj);
        DTO<Models.Outputs.CloseWODetailsDTO> CloseWorkOrderDetails(Input<Models.Inputs.CloseWODetails> obj);
        DTO<Models.Outputs.AddDeviceIDDTO> AddDeviceID(Input<Models.Inputs.AddDeviceID> obj);



        #region Sales & Mandoop
        DTO<Models.Outputs.GetTaskTypesDTO> GetTaskTypes(Input<Models.Inputs.GetTaskTypes> obj);
        DTO<Models.Outputs.GetTaskStatusDTO> GetTaskStatus(Input<Models.Inputs.GetTaskStatus> obj);
        DTO<Models.Outputs.GetSalesTaskByUserIDDTO> GetSalesTaskByUserID(Input<Models.Inputs.GetSalesTaskByUserID> obj);
        DTO<Models.Outputs.GetMandoopTaskByUserIDDTO> GetMandoopTaskByUserID(Input<Models.Inputs.GetMandoopTaskByUserID> obj);
        DTO<Models.Outputs.UpdateTaskSalesDTO> UpdateTaskSales(Input<Models.Inputs.UpdateTaskSales> obj);
        DTO<Models.Outputs.UpdateTaskMandoopDTO> UpdateTaskMandoop(Input<Models.Inputs.UpdateTaskMandoop> obj);
        DTO<Models.Outputs.CreateTaskSalesDTO> CreateTaskSales(Input<Models.Inputs.CreateTaskSales> obj);
        DTO<Models.Outputs.CreateTaskMandoopDTO> CreateTaskMandoop(Input<Models.Inputs.CreateTaskMandoop> obj);
        DTO<Models.Outputs.GetSearchTaskSalesDTO> GetSearchTaskSales(Input<Models.Inputs.GetSearchTaskSales> obj);
        DTO<Models.Outputs.GetSearchTaskMandoopDTO> GetSearchTaskMandoop(Input<Models.Inputs.GetSearchTaskMandoop> obj);
        #endregion

    }
}
