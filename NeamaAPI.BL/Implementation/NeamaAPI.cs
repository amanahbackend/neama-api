﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeamaAPI.BL.Interface;
using NeamaAPI.Models;
using NeamaAPI.DAL;
using NeamaAPI.Models.Outputs;
using System.Net;
using System.IO;

namespace NeamaAPI.BL.Implementation
{
    public class NeamaAPI : INeamaAPI
    {
        NeamaEntities db = new NeamaEntities();
        TabletUser tu = new TabletUser();


        #region Public Methods

        public DTO<Models.Outputs.GetRolesDTO> GetRoles(Input<Models.Inputs.Role> obj)
        {
            DTO<Models.Outputs.GetRolesDTO> dto = new DTO<Models.Outputs.GetRolesDTO>();
            Models.Outputs.GetRolesDTO resp = new Models.Outputs.GetRolesDTO();
            dto.objname = "GetRoles";
            try
            {
                resp = getroles();
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.GetTechniciansDTO> GetTechnicians(Input<Models.Inputs.GetTechnicians> obj)
        {
            DTO<Models.Outputs.GetTechniciansDTO> dto = new DTO<Models.Outputs.GetTechniciansDTO>();
            Models.Outputs.GetTechniciansDTO resp = new Models.Outputs.GetTechniciansDTO();
            dto.objname = "GetTechnicians";
            try
            {
                resp = gettechnicians();
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.LoginDTO> Login(Input<Models.Inputs.Login> obj)
        {
            DTO<Models.Outputs.LoginDTO> dto = new DTO<Models.Outputs.LoginDTO>();
            Models.Outputs.LoginDTO resp = new Models.Outputs.LoginDTO();
            dto.objname = "Login";

            try
            {
                if (string.IsNullOrEmpty(obj.input.username) || string.IsNullOrEmpty(obj.input.password))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                if (!isvalidlogin(obj.input.username, obj.input.password))
                {
                    dto.status = new Models.Status(1040);
                    return dto;
                }

                resp.userid = tu.ID.ToString();
                resp.mobile = tu.Mobile;
                resp.name = tu.Name;
                resp.role = gettabletrolebyid(Convert.ToInt32(tu.TabletRoleID));

                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.GetWOBySupervisorDTO> GetWOBySupervisor(Input<Models.Inputs.GetWOBySupervisor> obj)
        {
            DTO<Models.Outputs.GetWOBySupervisorDTO> dto = new DTO<Models.Outputs.GetWOBySupervisorDTO>();
            Models.Outputs.GetWOBySupervisorDTO resp = new Models.Outputs.GetWOBySupervisorDTO();
            dto.objname = "GetWOBySupervisor";
            try
            {
                if (string.IsNullOrEmpty(obj.input.SupervisorID))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = getwobysupervisorid(Convert.ToInt32(obj.input.SupervisorID));
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.SaveReportTechnicianDTO> SaveReportTechnician(Input<Models.Inputs.SaveReportTechnician> obj)
        {
            DTO<Models.Outputs.SaveReportTechnicianDTO> dto = new DTO<Models.Outputs.SaveReportTechnicianDTO>();
            Models.Outputs.SaveReportTechnicianDTO resp = new Models.Outputs.SaveReportTechnicianDTO();
            dto.objname = "SaveReportTechnician";
            try
            {
                //if (string.IsNullOrEmpty(obj.input.SupervisorID))
                //{
                //    dto.status = new Models.Status(800);
                //    return dto;
                //}

                resp = savereporttech(obj.input);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.GetTrackTypeDTO> GetTrackType(Input<Models.Inputs.GetTrackType> obj)
        {
            DTO<Models.Outputs.GetTrackTypeDTO> dto = new DTO<Models.Outputs.GetTrackTypeDTO>();
            Models.Outputs.GetTrackTypeDTO resp = new Models.Outputs.GetTrackTypeDTO();
            dto.objname = "GetTrackType";
            try
            {
                resp = gettracktypes();
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.TrackDailyLogDTO> TrackDailyLog(Input<Models.Inputs.TrackDailyLog> obj)
        {
            DTO<Models.Outputs.TrackDailyLogDTO> dto = new DTO<Models.Outputs.TrackDailyLogDTO>();
            Models.Outputs.TrackDailyLogDTO resp = new Models.Outputs.TrackDailyLogDTO();
            dto.objname = "TrackDailyLog";
            try
            {
                if (string.IsNullOrEmpty(obj.input.userid) || string.IsNullOrEmpty(obj.input.tracktypeid) || string.IsNullOrEmpty(obj.input.roleid)
                    || string.IsNullOrEmpty(obj.input.latitude) || string.IsNullOrEmpty(obj.input.longtitude))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = trackdaily(Convert.ToInt32(obj.input.userid), Convert.ToInt32(obj.input.roleid), obj.input.latitude
                    , obj.input.longtitude, Convert.ToInt32(obj.input.tracktypeid));
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.CloseWODTO> CloseWorkOrder(Input<Models.Inputs.CloseWO> obj)
        {
            DTO<Models.Outputs.CloseWODTO> dto = new DTO<Models.Outputs.CloseWODTO>();
            Models.Outputs.CloseWODTO resp = new Models.Outputs.CloseWODTO();
            dto.objname = "CloseWorkOrder";
            try
            {
                if (string.IsNullOrEmpty(obj.input.typeid) || string.IsNullOrEmpty(obj.input.woid))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = closewo(obj.input.typeid, Convert.ToInt32(obj.input.woid));
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.CloseWODetailsDTO> CloseWorkOrderDetails(Input<Models.Inputs.CloseWODetails> obj)
        {
            DTO<Models.Outputs.CloseWODetailsDTO> dto = new DTO<Models.Outputs.CloseWODetailsDTO>();
            Models.Outputs.CloseWODetailsDTO resp = new Models.Outputs.CloseWODetailsDTO();
            dto.objname = "CloseWorkOrderDetails";
            try
            {
                if (string.IsNullOrEmpty(obj.input.typeid) || string.IsNullOrEmpty(obj.input.woid) || string.IsNullOrEmpty(obj.input.quantity))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = closewodetails(obj.input.typeid, Convert.ToInt32(obj.input.woid), obj.input.quantity);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.GetTaskTypesDTO> GetTaskTypes(Input<Models.Inputs.GetTaskTypes> obj)
        {
            DTO<Models.Outputs.GetTaskTypesDTO> dto = new DTO<Models.Outputs.GetTaskTypesDTO>();
            Models.Outputs.GetTaskTypesDTO resp = new Models.Outputs.GetTaskTypesDTO();
            dto.objname = "GetTaskTypes";
            try
            {
                resp = getstacktypes();
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.GetTaskStatusDTO> GetTaskStatus(Input<Models.Inputs.GetTaskStatus> obj)
        {
            DTO<Models.Outputs.GetTaskStatusDTO> dto = new DTO<Models.Outputs.GetTaskStatusDTO>();
            Models.Outputs.GetTaskStatusDTO resp = new Models.Outputs.GetTaskStatusDTO();
            dto.objname = "GetTaskStatus";
            try
            {
                resp = gettaskstatus();
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.GetSalesTaskByUserIDDTO> GetSalesTaskByUserID(Input<Models.Inputs.GetSalesTaskByUserID> obj)
        {
            DTO<Models.Outputs.GetSalesTaskByUserIDDTO> dto = new DTO<Models.Outputs.GetSalesTaskByUserIDDTO>();
            Models.Outputs.GetSalesTaskByUserIDDTO resp = new Models.Outputs.GetSalesTaskByUserIDDTO();
            dto.objname = "GetSalesTaskByUserID";
            try
            {
                if (string.IsNullOrEmpty(obj.input.userid))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = gettaskbyuserid(Convert.ToInt32(obj.input.userid));
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.GetMandoopTaskByUserIDDTO> GetMandoopTaskByUserID(Input<Models.Inputs.GetMandoopTaskByUserID> obj)
        {
            DTO<Models.Outputs.GetMandoopTaskByUserIDDTO> dto = new DTO<Models.Outputs.GetMandoopTaskByUserIDDTO>();
            Models.Outputs.GetMandoopTaskByUserIDDTO resp = new Models.Outputs.GetMandoopTaskByUserIDDTO();
            dto.objname = "GetMandoopTaskByUserID";
            try
            {
                if (string.IsNullOrEmpty(obj.input.userid))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = getmandooptaskbyuserid(Convert.ToInt32(obj.input.userid));
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.UpdateTaskSalesDTO> UpdateTaskSales(Input<Models.Inputs.UpdateTaskSales> obj)
        {
            DTO<Models.Outputs.UpdateTaskSalesDTO> dto = new DTO<Models.Outputs.UpdateTaskSalesDTO>();
            Models.Outputs.UpdateTaskSalesDTO resp = new Models.Outputs.UpdateTaskSalesDTO();
            dto.objname = "UpdateTaskSales";
            try
            {
                if (string.IsNullOrEmpty(obj.input.tasksalesid) || string.IsNullOrEmpty(obj.input.taskstatusid))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = updatetasksalesbyid(obj.input);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;

        }
        public DTO<Models.Outputs.UpdateTaskMandoopDTO> UpdateTaskMandoop(Input<Models.Inputs.UpdateTaskMandoop> obj)
        {
            DTO<Models.Outputs.UpdateTaskMandoopDTO> dto = new DTO<Models.Outputs.UpdateTaskMandoopDTO>();
            Models.Outputs.UpdateTaskMandoopDTO resp = new Models.Outputs.UpdateTaskMandoopDTO();
            dto.objname = "UpdateTaskMandoop";
            try
            {
                if (string.IsNullOrEmpty(obj.input.taskmandoopid) || string.IsNullOrEmpty(obj.input.taskstatusid))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = updatetaskmandoopbyid(obj.input);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.CreateTaskSalesDTO> CreateTaskSales(Input<Models.Inputs.CreateTaskSales> obj)
        {
            DTO<Models.Outputs.CreateTaskSalesDTO> dto = new DTO<Models.Outputs.CreateTaskSalesDTO>();
            Models.Outputs.CreateTaskSalesDTO resp = new Models.Outputs.CreateTaskSalesDTO();
            dto.objname = "CreateTaskSales";
            try
            {
                if (string.IsNullOrEmpty(obj.input.tasktypeid) || string.IsNullOrEmpty(obj.input.taskstatusid)
                    || string.IsNullOrEmpty(obj.input.customername) || string.IsNullOrEmpty(obj.input.datetime))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = createtasksales(obj.input);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.CreateTaskMandoopDTO> CreateTaskMandoop(Input<Models.Inputs.CreateTaskMandoop> obj)
        {
            DTO<Models.Outputs.CreateTaskMandoopDTO> dto = new DTO<Models.Outputs.CreateTaskMandoopDTO>();
            Models.Outputs.CreateTaskMandoopDTO resp = new Models.Outputs.CreateTaskMandoopDTO();
            dto.objname = "CreateTaskMandoop";
            try
            {
                if (string.IsNullOrEmpty(obj.input.tasktypeid) || string.IsNullOrEmpty(obj.input.taskstatusid)
                    || string.IsNullOrEmpty(obj.input.customername) || string.IsNullOrEmpty(obj.input.datetime))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = createtaskmandoop(obj.input);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.GetSearchTaskSalesDTO> GetSearchTaskSales(Input<Models.Inputs.GetSearchTaskSales> obj)
        {
            DTO<Models.Outputs.GetSearchTaskSalesDTO> dto = new DTO<Models.Outputs.GetSearchTaskSalesDTO>();
            Models.Outputs.GetSearchTaskSalesDTO resp = new Models.Outputs.GetSearchTaskSalesDTO();
            dto.objname = "GetSearchTaskSales";
            try
            {
                if (string.IsNullOrEmpty(obj.input.assignfromdate) || string.IsNullOrEmpty(obj.input.assigntodate) || string.IsNullOrEmpty(obj.input.userid)
                    || string.IsNullOrEmpty(obj.input.typeid) || string.IsNullOrEmpty(obj.input.taskstatusid))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = getsearchtasksales(obj.input);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.GetSearchTaskMandoopDTO> GetSearchTaskMandoop(Input<Models.Inputs.GetSearchTaskMandoop> obj)
        {
            DTO<Models.Outputs.GetSearchTaskMandoopDTO> dto = new DTO<Models.Outputs.GetSearchTaskMandoopDTO>();
            Models.Outputs.GetSearchTaskMandoopDTO resp = new Models.Outputs.GetSearchTaskMandoopDTO();
            dto.objname = "GetSearchTaskMandoop";
            try
            {
                if (string.IsNullOrEmpty(obj.input.assignfromdate) || string.IsNullOrEmpty(obj.input.assigntodate) || string.IsNullOrEmpty(obj.input.userid)
                   || string.IsNullOrEmpty(obj.input.typeid) || string.IsNullOrEmpty(obj.input.taskstatusid))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = getsearchtaskmandoop(obj.input);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        public DTO<Models.Outputs.AddDeviceIDDTO> AddDeviceID(Input<Models.Inputs.AddDeviceID> obj)
        {
            DTO<Models.Outputs.AddDeviceIDDTO> dto = new DTO<Models.Outputs.AddDeviceIDDTO>();
            Models.Outputs.AddDeviceIDDTO resp = new Models.Outputs.AddDeviceIDDTO();
            dto.objname = "AddDeviceID";
            try
            {
                if (string.IsNullOrEmpty(obj.input.userid) || string.IsNullOrEmpty(obj.input.deviceid))
                {
                    dto.status = new Models.Status(800);
                    return dto;
                }

                resp = adddevicebyuserid(Convert.ToInt32(obj.input.userid), obj.input.deviceid);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }
        #endregion

        #region Private Methods


        public static String SendNotificationFromFirebaseCloud()
        {
            var result = "-1";
            var webAddr = "https://fcm.googleapis.com/fcm/send";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "key=AIzaSyBs4W8oTZdXWiNZXeZiUgND2OpjX7RA0IM");
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string strNJson = @"{
                    ""to"": ""/topics/ServiceNow"",
                    ""notification"": {
                        ""title"": ""New Task Assigned"",
                            ""text"": ""check the application to know the task"",
                        ""sound"":""default""
                          }
                            }";
                streamWriter.Write(strNJson);
                streamWriter.Flush();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

        private Models.Outputs.AddDeviceIDDTO adddevicebyuserid(int userid, string deviceid)
        {
            Models.Outputs.AddDeviceIDDTO resp = new AddDeviceIDDTO();

            var row = db.TabletUsers.Where(x => x.ID == userid && x.isDeleted == false).FirstOrDefault();

            if (row != null)
            {
                row.DeviceID = deviceid;
                db.SaveChanges();
            }

            return resp;
        }
        private Models.Outputs.GetSearchTaskSalesDTO getsearchtasksales(Models.Inputs.GetSearchTaskSales obj)
        {
            Models.Outputs.GetSearchTaskSalesDTO resp = new GetSearchTaskSalesDTO();
            List<SalesTask> stlst = new List<SalesTask>();

            int userid = Convert.ToInt32(obj.userid);
            int tasktypeid = Convert.ToInt32(obj.typeid);
            int taskstatusid = Convert.ToInt32(obj.taskstatusid);
            DateTime datefrom = Convert.ToDateTime(obj.assignfromdate);
            DateTime dateto = Convert.ToDateTime(obj.assigntodate);

            var rows = db.TasksSales.Where(x => x.TaskForUserID == userid && (x.TaskStatusID == taskstatusid || taskstatusid == 0)
            && (x.DateTime >= datefrom || obj.assignfromdate == "1/1/1900 00:00:00") && (x.DateTime <= dateto || obj.assigntodate == "1/1/1900 23:59:59")
            && (x.TaskTypeID == tasktypeid || tasktypeid == 0) && (x.CustomerName.Contains(obj.customername) || obj.customername == "") && x.IsDeleted == false).OrderByDescending(x => x.ID).ToList();
            foreach (var row in rows)
            {
                SalesTask st = new SalesTask();
                st.customername = row.CustomerName;
                st.datetime = row.DateTime.ToString();
                st.description = row.Description;
                st.id = row.ID.ToString();
                st.remark = row.Remark;
                st.taskforuserid = row.TaskForUserID.ToString();
                st.taskstatus = gettaskstatusbyid(Convert.ToInt32(row.TaskStatusID));
                st.tasktype = gettasktypebyid(Convert.ToInt32(row.TaskTypeID));
                st.images = getimagesbytasksalesid(row.ID);
                stlst.Add(st);
            }
            resp.salestasks = stlst;

            return resp;
        }
        private Models.Outputs.GetSearchTaskMandoopDTO getsearchtaskmandoop(Models.Inputs.GetSearchTaskMandoop obj)
        {
            Models.Outputs.GetSearchTaskMandoopDTO resp = new GetSearchTaskMandoopDTO();
            List<MandoopTask> mtlst = new List<MandoopTask>();
            int userid = Convert.ToInt32(obj.userid);
            int tasktypeid = Convert.ToInt32(obj.typeid);
            int taskstatusid = Convert.ToInt32(obj.taskstatusid);
            DateTime datefrom = Convert.ToDateTime(obj.assignfromdate);
            DateTime dateto = Convert.ToDateTime(obj.assigntodate);

            var rows = db.TaskMandoops.Where(x => x.TaskForUserID == userid && (x.TaskStatusID == taskstatusid || taskstatusid == 0)
            && (x.DateTime >= datefrom || obj.assignfromdate == "1/1/1900 00:00:00") && (x.DateTime <= dateto || obj.assigntodate == "1/1/1900 23:59:59")
            && (x.TaskTypeID == tasktypeid || tasktypeid == 0) && (x.CustomerName.Contains(obj.customername) || obj.customername == "") && x.IsDeleted == false).OrderByDescending(x => x.ID).ToList();
            foreach (var row in rows)
            {
                MandoopTask mt = new MandoopTask();
                mt.customername = row.CustomerName;
                mt.datetime = row.DateTime.ToString();
                mt.description = row.Description;
                mt.id = row.ID.ToString();
                mt.remark = row.Remark;
                mt.taskforuserid = row.TaskForUserID.ToString();
                mt.taskstatus = gettaskstatusbyid(Convert.ToInt32(row.TaskStatusID));
                mt.tasktype = gettasktypebyid(Convert.ToInt32(row.TaskTypeID));
                mt.images = getimagesbytaskmandoopid(row.ID);
                mtlst.Add(mt);
            }
            resp.mandooptasks = mtlst;

            return resp;
        }
        private Models.Outputs.CreateTaskMandoopDTO createtaskmandoop(Models.Inputs.CreateTaskMandoop obj)
        {
            Models.Outputs.CreateTaskMandoopDTO resp = new CreateTaskMandoopDTO();
            int tasktypeid = Convert.ToInt32(obj.tasktypeid);
            int taskstatusid = Convert.ToInt32(obj.taskstatusid);
            int userid = Convert.ToInt32(obj.userid);

            TaskMandoop tm = new TaskMandoop();
            tm.CreatedDate = DateTime.Now;
            tm.Description = obj.description;
            tm.TaskForUserID = userid;
            tm.TaskStatusID = taskstatusid;
            tm.TaskTypeID = tasktypeid;
            tm.CustomerName = obj.customername;
            tm.DateTime = Convert.ToDateTime(obj.datetime);
            tm.Remark = obj.remark;
            tm.CreatedBy = gettabletusernamebyid(userid);
            db.TaskMandoops.Add(tm);
            db.SaveChanges();

            foreach (var imgs in obj.images)
            {
                TaskMandoopImage tmi = new TaskMandoopImage();
                tmi.TaskMandoopID = tm.ID;
                tmi.ImagePath = Contants.getLocationBySavingImage(imgs.img, 2);         // 1: sales task , 2: mandoop task
                tmi.CreatedDate = DateTime.Now;
                db.TaskMandoopImages.Add(tmi);
                db.SaveChanges();
            }
            MandoopTask mt = new MandoopTask();
            mt.customername = tm.CustomerName;
            mt.datetime = tm.DateTime.ToString();
            mt.description = tm.Description;
            mt.id = tm.ID.ToString();
            mt.images = getimagesbytaskmandoopid(tm.ID);
            mt.remark = tm.Remark;
            mt.taskforuserid = tm.TaskForUserID.ToString();
            mt.taskstatus = gettaskstatusbyid(Convert.ToInt32(tm.TaskStatusID));
            mt.tasktype = gettasktypebyid(Convert.ToInt32(tm.TaskTypeID));

            resp.mandooptask = mt;
            return resp;
        }
        private Models.Outputs.CreateTaskSalesDTO createtasksales(Models.Inputs.CreateTaskSales obj)
        {
            Models.Outputs.CreateTaskSalesDTO resp = new CreateTaskSalesDTO();
            int tasktypeid = Convert.ToInt32(obj.tasktypeid);
            int taskstatusid = Convert.ToInt32(obj.taskstatusid);
            int userid = Convert.ToInt32(obj.userid);

            TasksSale ts = new TasksSale();
            ts.CreatedDate = DateTime.Now;
            ts.Description = obj.description;
            ts.TaskForUserID = userid;
            ts.TaskStatusID = taskstatusid;
            ts.TaskTypeID = tasktypeid;
            ts.CustomerName = obj.customername;
            ts.DateTime = Convert.ToDateTime(obj.datetime);
            ts.Remark = obj.remark;
            ts.CreatedBy = gettabletusernamebyid(userid);
            db.TasksSales.Add(ts);
            db.SaveChanges();

            foreach (var imgs in obj.images)
            {
                TaskSalesImage tsi = new TaskSalesImage();
                tsi.TaskSalesID = ts.ID;
                tsi.ImagePath = Contants.getLocationBySavingImage(imgs.img, 1);         // 1: sales task , 2: mandoop task
                tsi.CreatedDate = DateTime.Now;
                db.TaskSalesImages.Add(tsi);
                db.SaveChanges();
            }

            SalesTask st = new SalesTask();
            st.customername = ts.CustomerName;
            st.datetime = ts.DateTime.ToString();
            st.description = ts.Description;
            st.id = ts.ID.ToString();
            st.images = getimagesbytasksalesid(ts.ID);
            st.remark = ts.Remark;
            st.taskforuserid = ts.TaskForUserID.ToString();
            st.taskstatus = gettaskstatusbyid(Convert.ToInt32(ts.TaskStatusID));
            st.tasktype = gettasktypebyid(Convert.ToInt32(ts.TaskTypeID));

            resp.salestask = st;

            return resp;
        }
        private string gettabletusernamebyid(int id)
        {
            var row = db.TabletUsers.Where(x => x.ID == id).FirstOrDefault();
            if (row != null)
            {
                return row.Name;
            }
            return "";
        }
        private Models.Outputs.UpdateTaskMandoopDTO updatetaskmandoopbyid(Models.Inputs.UpdateTaskMandoop obj)
        {
            Models.Outputs.UpdateTaskMandoopDTO resp = new UpdateTaskMandoopDTO();

            int taskmandoopid = Convert.ToInt32(obj.taskmandoopid);
            int taskstatusid = Convert.ToInt32(obj.taskstatusid);

            var row = db.TaskMandoops.Where(x => x.ID == taskmandoopid && x.IsDeleted == false).FirstOrDefault();
            if (row != null)
            {
                row.Remark = obj.remark;
                row.TaskStatusID = taskstatusid;
                db.SaveChanges();

                foreach (var imgs in obj.images)
                {
                    TaskMandoopImage tmi = new TaskMandoopImage();
                    tmi.TaskMandoopID = row.ID;
                    tmi.ImagePath = Contants.getLocationBySavingImage(imgs.img, 2);         // 1: sales task , 2: mandoop task
                    tmi.CreatedDate = DateTime.Now;
                    db.TaskMandoopImages.Add(tmi);
                    db.SaveChanges();
                }
                MandoopTask mt = new MandoopTask();
                mt.customername = row.CustomerName;
                mt.datetime = row.DateTime.ToString();
                mt.description = row.Description;
                mt.id = row.ID.ToString();
                mt.images = getimagesbytaskmandoopid(row.ID);
                mt.remark = row.Remark;
                mt.taskforuserid = row.TaskForUserID.ToString();
                mt.taskstatus = gettaskstatusbyid(Convert.ToInt32(row.TaskStatusID));
                mt.tasktype = gettasktypebyid(Convert.ToInt32(row.TaskTypeID));

                resp.mandooptask = mt;
                return resp;
            }
            return resp;
        }
        private Models.Outputs.UpdateTaskSalesDTO updatetasksalesbyid(Models.Inputs.UpdateTaskSales obj)
        {
            Models.Outputs.UpdateTaskSalesDTO resp = new UpdateTaskSalesDTO();

            int tasksalesid = Convert.ToInt32(obj.tasksalesid);
            int taskstatusid = Convert.ToInt32(obj.taskstatusid);

            var row = db.TasksSales.Where(x => x.ID == tasksalesid && x.IsDeleted == false).FirstOrDefault();

            if (row != null)
            {
                row.Remark = obj.remark;
                row.TaskStatusID = taskstatusid;
                db.SaveChanges();

                foreach (var imgs in obj.images)
                {
                    TaskSalesImage tsi = new TaskSalesImage();
                    tsi.TaskSalesID = row.ID;
                    tsi.ImagePath = Contants.getLocationBySavingImage(imgs.img, 1);         // 1: sales task , 2: mandoop task
                    tsi.CreatedDate = DateTime.Now;
                    db.TaskSalesImages.Add(tsi);
                    db.SaveChanges();
                }

                SalesTask st = new SalesTask();
                st.customername = row.CustomerName;
                st.datetime = row.DateTime.ToString();
                st.description = row.Description;
                st.id = row.ID.ToString();
                st.images = getimagesbytasksalesid(row.ID);
                st.remark = row.Remark;
                st.taskforuserid = row.TaskForUserID.ToString();
                st.taskstatus = gettaskstatusbyid(Convert.ToInt32(row.TaskStatusID));
                st.tasktype = gettasktypebyid(Convert.ToInt32(row.TaskTypeID));

                resp.salestask = st;

                return resp;
            }

            return resp;
        }
        private List<Images> getimagesbytasksalesid(int tasksalesid)
        {
            List<Images> imglst = new List<Images>();

            var rows = db.TaskSalesImages.Where(x => x.TaskSalesID == tasksalesid).ToList();

            foreach (var row in rows)
            {
                Images img = new Images();
                img.imgpath = row.ImagePath;
                imglst.Add(img);
            }
            return imglst;
        }
        private List<Images> getimagesbytaskmandoopid(int taskmandoopid)
        {
            List<Images> imglst = new List<Images>();

            var rows = db.TaskMandoopImages.Where(x => x.TaskMandoopID == taskmandoopid).ToList();

            foreach (var row in rows)
            {
                Images img = new Images();
                img.imgpath = row.ImagePath;
                imglst.Add(img);
            }
            return imglst;
        }
        private Models.Outputs.GetMandoopTaskByUserIDDTO getmandooptaskbyuserid(int userid)
        {
            Models.Outputs.GetMandoopTaskByUserIDDTO resp = new GetMandoopTaskByUserIDDTO();
            List<MandoopTask> mtlst = new List<MandoopTask>();

            var rows = db.TaskMandoops.Where(x => x.TaskForUserID == userid && x.TaskStatusID != 3 && x.IsDeleted == false).OrderByDescending(x => x.ID).ToList();
            foreach (var row in rows)
            {
                MandoopTask mt = new MandoopTask();
                mt.customername = row.CustomerName;
                mt.datetime = row.DateTime.ToString();
                mt.description = row.Description;
                mt.id = row.ID.ToString();
                mt.remark = row.Remark;
                mt.taskforuserid = row.TaskForUserID.ToString();
                mt.taskstatus = gettaskstatusbyid(Convert.ToInt32(row.TaskStatusID));
                mt.tasktype = gettasktypebyid(Convert.ToInt32(row.TaskTypeID));
                mt.images = getimagesbytaskmandoopid(row.ID);
                mtlst.Add(mt);
            }
            resp.mandooptasks = mtlst;

            return resp;
        }
        private Models.Outputs.GetSalesTaskByUserIDDTO gettaskbyuserid(int userid)
        {
            Models.Outputs.GetSalesTaskByUserIDDTO resp = new GetSalesTaskByUserIDDTO();
            List<SalesTask> stlst = new List<SalesTask>();

            var rows = db.TasksSales.Where(x => x.TaskForUserID == userid && x.TaskStatusID != 3 && x.IsDeleted == false).OrderByDescending(x => x.ID).ToList();
            foreach (var row in rows)
            {
                SalesTask st = new SalesTask();
                st.customername = row.CustomerName;
                st.datetime = row.DateTime.ToString();
                st.description = row.Description;
                st.id = row.ID.ToString();
                st.remark = row.Remark;
                st.taskforuserid = row.TaskForUserID.ToString();
                st.taskstatus = gettaskstatusbyid(Convert.ToInt32(row.TaskStatusID));
                st.tasktype = gettasktypebyid(Convert.ToInt32(row.TaskTypeID));
                st.images = getimagesbytasksalesid(row.ID);
                stlst.Add(st);
            }
            resp.salestasks = stlst;

            return resp;
        }
        private Models.Outputs.TaskStatus gettaskstatusbyid(int id)
        {
            Models.Outputs.TaskStatus resp = new Models.Outputs.TaskStatus();

            var row = db.TaskStatus.Where(x => x.ID == id).FirstOrDefault();
            if (row != null)
            {
                resp.id = row.ID.ToString();
                resp.taskstatusname = row.StatusName;
            }
            return resp;
        }
        private Models.Outputs.GetTaskStatusDTO gettaskstatus()
        {
            Models.Outputs.GetTaskStatusDTO resp = new GetTaskStatusDTO();
            List<Models.Outputs.TaskStatus> tslst = new List<Models.Outputs.TaskStatus>();

            var rows = db.TaskStatus.ToList();
            foreach (var row in rows)
            {
                Models.Outputs.TaskStatus ts = new Models.Outputs.TaskStatus();
                ts.id = row.ID.ToString();
                ts.taskstatusname = row.StatusName;
                tslst.Add(ts);
            }
            resp.taskstatus = tslst;

            return resp;
        }
        private Models.Outputs.TaskTypes gettasktypebyid(int id)
        {
            Models.Outputs.TaskTypes resp = new TaskTypes();

            var row = db.TaskTypes.Where(x => x.ID == id).FirstOrDefault();
            if (row != null)
            {
                resp.id = row.ID.ToString();
                resp.typename = row.TypeName;
            }

            return resp;
        }
        private Models.Outputs.GetTaskTypesDTO getstacktypes()
        {
            Models.Outputs.GetTaskTypesDTO resp = new GetTaskTypesDTO();
            List<TaskTypes> ttlst = new List<TaskTypes>();

            var rows = db.TaskTypes.ToList();

            foreach (var row in rows)
            {
                TaskTypes tt = new TaskTypes();
                tt.id = row.ID.ToString();
                tt.typename = row.TypeName;

                ttlst.Add(tt);
            }
            resp.tasktypes = ttlst;
            return resp;
        }
        private Models.Outputs.CloseWODTO closewo(string typeid, int woid)
        {
            Models.Outputs.CloseWODTO resp = new CloseWODTO();

            if (typeid == "1") //doortype
            {
                var row = db.WO_Door.Where(x => x.ID == woid && x.isCompleted == false && x.isDeleted == false).FirstOrDefault();
                if (row != null)
                {
                    row.isCompleted = true;
                    row.CompletedDate = DateTime.Now;
                    db.SaveChanges();

                    resp.isCompleted = row.isCompleted.ToString();
                    resp.type = row.isCompleted.ToString();
                    resp.woid = row.isCompleted.ToString();
                }
            }
            else if (typeid == "2")  //aluminiumtype
            {
                var row = db.WO_Aluminium.Where(x => x.ID == woid && x.isCompleted == false && x.isDeleted == false).FirstOrDefault();
                if (row != null)
                {
                    row.isCompleted = true;
                    row.CompletedDate = DateTime.Now;
                    db.SaveChanges();

                    resp.isCompleted = row.isCompleted.ToString();
                    resp.type = row.isCompleted.ToString();
                    resp.woid = row.isCompleted.ToString();
                }
            }
            else if (typeid == "3")  //tanktype
            {
                var row = db.WO_Tanks.Where(x => x.ID == woid && x.isCompleted == false && x.isDeleted == false).FirstOrDefault();
                if (row != null)
                {
                    row.isCompleted = true;
                    row.CompletedDate = DateTime.Now;
                    db.SaveChanges();

                    resp.isCompleted = row.isCompleted.ToString();
                    resp.type = row.isCompleted.ToString();
                    resp.woid = row.isCompleted.ToString();
                }
            }
            else if (typeid == "4")   //grpsectiontype
            {
                var row = db.WO_GRPSection.Where(x => x.ID == woid && x.isCompleted == false && x.isDeleted == false).FirstOrDefault();
                if (row != null)
                {
                    row.isCompleted = true;
                    row.CompletedDate = DateTime.Now;
                    db.SaveChanges();

                    resp.isCompleted = row.isCompleted.ToString();
                    resp.type = row.isCompleted.ToString();
                    resp.woid = row.isCompleted.ToString();
                }
            }
            else if (typeid == "5")    //glassfybretype
            {
                var row = db.WO_GFHandLayer.Where(x => x.ID == woid && x.isCompleted == false && x.isDeleted == false).FirstOrDefault();
                if (row != null)
                {
                    row.isCompleted = true;
                    row.CompletedDate = DateTime.Now;
                    db.SaveChanges();

                    resp.isCompleted = row.isCompleted.ToString();
                    resp.type = row.isCompleted.ToString();
                    resp.woid = row.isCompleted.ToString();
                }
            }

            return resp;
        }
        private Models.Outputs.CloseWODetailsDTO closewodetails(string typeid, int woid, string quantity)
        {
            Models.Outputs.CloseWODetailsDTO resp = new CloseWODetailsDTO();

            if (typeid == "1")  //doortype
            {
                var row = db.WO_Door_Details.Where(x => x.ID == woid && x.Quantity == quantity && x.isCompleted == false && x.isDeleted == false).FirstOrDefault();
                if (row != null)
                {
                    row.isCompleted = true;
                    row.CompletedDate = DateTime.Now;
                    db.SaveChanges();

                    resp.isCompleted = row.isCompleted.ToString();
                    resp.type = typeid;
                    resp.woid = row.ID.ToString();
                    resp.quantity = row.Quantity.ToString();
                }
            }
            else if (typeid == "2")   //aluminiumtype
            {
                var row = db.WO_Aluminium_Details.Where(x => x.ID == woid && x.Quantity == quantity && x.isCompleted == false && x.isDeleted == false).FirstOrDefault();
                if (row != null)
                {
                    row.isCompleted = true;
                    row.CompletedDate = DateTime.Now;
                    db.SaveChanges();

                    resp.isCompleted = row.isCompleted.ToString();
                    resp.type = typeid;
                    resp.woid = row.ID.ToString();
                    resp.quantity = row.Quantity.ToString();
                }
            }
            else if (typeid == "3")   //tanktype
            {
                var row = db.WO_Tanks_Details.Where(x => x.ID == woid && x.Quantity == quantity && x.isCompleted == false && x.isDeleted == false).FirstOrDefault();
                if (row != null)
                {
                    row.isCompleted = true;
                    row.CompletedDate = DateTime.Now;
                    db.SaveChanges();

                    resp.isCompleted = row.isCompleted.ToString();
                    resp.type = typeid;
                    resp.woid = row.ID.ToString();
                    resp.quantity = row.Quantity.ToString();
                }
            }
            else if (typeid == "4")        //grpsectiontype
            {
                var row = db.WO_GRPSection_Details.Where(x => x.ID == woid && x.Quantity == quantity && x.isCompleted == false && x.isDeleted == false).FirstOrDefault();
                if (row != null)
                {
                    row.isCompleted = true;
                    row.CompletedDate = DateTime.Now;
                    db.SaveChanges();

                    resp.isCompleted = row.isCompleted.ToString();
                    resp.type = typeid;
                    resp.woid = row.ID.ToString();
                    resp.quantity = row.Quantity.ToString();
                }
            }
            else if (typeid == "5")           //glassfybretype
            {
                var row = db.WO_GFHandLayer_Details.Where(x => x.ID == woid && x.Quantity == quantity && x.isCompleted == false && x.isDeleted == false).FirstOrDefault();
                if (row != null)
                {
                    row.isCompleted = true;
                    row.CompletedDate = DateTime.Now;
                    db.SaveChanges();

                    resp.isCompleted = row.isCompleted.ToString();
                    resp.type = typeid;
                    resp.woid = row.ID.ToString();
                    resp.quantity = row.Quantity.ToString();
                }
            }

            return resp;
        }
        private Models.Outputs.GetTrackTypeDTO gettracktypes()
        {
            Models.Outputs.GetTrackTypeDTO resp = new GetTrackTypeDTO();
            List<TrackTypes> tlst = new List<TrackTypes>();

            var rows = db.TrackTypes.ToList();

            foreach (var row in rows)
            {
                TrackTypes tt = new TrackTypes();
                tt.id = row.ID.ToString();
                tt.name = row.Name;
                tlst.Add(tt);
            }
            resp.tracktypes = tlst;

            return resp;
        }
        private Models.Outputs.TrackDailyLogDTO trackdaily(int userid, int rolied, string lati, string longtitude, int tracktypeid)
        {
            Models.Outputs.TrackDailyLogDTO resp = new TrackDailyLogDTO();

            DailyLog dl = new DailyLog();
            dl.CreatedDate = DateTime.Now;
            dl.TabletUserID = userid;
            dl.TabletRoleID = rolied;
            dl.Latitude = lati;
            dl.Longtitude = longtitude;
            dl.TrackTypeID = tracktypeid;
            db.DailyLogs.Add(dl);
            db.SaveChanges();

            return resp;

        }
        private Models.Outputs.SaveReportTechnicianDTO savereporttech(Models.Inputs.SaveReportTechnician obj)
        {
            Models.Outputs.SaveReportTechnicianDTO resp = new SaveReportTechnicianDTO();

            if (obj.type == "doortype")
            {
                ReportwoDoor rptdr = new ReportwoDoor();
                rptdr.ActionDone = obj.actiondone;
                rptdr.CreatedDate = DateTime.Now;
                rptdr.SupervisorID = Convert.ToInt32(obj.supervisorid);
                rptdr.TechnicianID = Convert.ToInt32(obj.technicianid);
                rptdr.WOID = Convert.ToInt32(obj.woid);
                rptdr.WOQuantity = Convert.ToInt32(obj.woquantity);

                db.ReportwoDoors.Add(rptdr);
                db.SaveChanges();

                if (!string.IsNullOrEmpty(obj.doortype.id))
                {
                    int id = Convert.ToInt32(obj.doortype.id);

                    var row = db.WO_Door_Details.Where(x => x.ID == id).FirstOrDefault();

                    if (row != null)
                    {
                        row.IsCleaning = obj.doortype.iscleaning.ToLower().Equals("true") ? true : false;
                        row.IsCollection = obj.doortype.iscollection.ToLower().Equals("true") ? true : false;
                        row.isCompleted = obj.doortype.iscompleted.ToLower().Equals("true") ? true : false;
                        row.IsCuttingForGlass = obj.doortype.iscutting.ToLower().Equals("true") ? true : false;
                        row.IsDoor = obj.doortype.isdoor.ToLower().Equals("true") ? true : false;
                        row.IsFoom = obj.doortype.isfoom.ToLower().Equals("true") ? true : false;
                        row.IsFrameInside = obj.doortype.isframeinside.ToLower().Equals("true") ? true : false;
                        row.IsFrameOutside = obj.doortype.isframeoutside.ToLower().Equals("true") ? true : false;
                        row.IsGlassFitting = obj.doortype.isglassfitting.ToLower().Equals("true") ? true : false;
                        row.IsPainting = obj.doortype.ispainting.ToLower().Equals("true") ? true : false;
                        row.isRepair = obj.doortype.isrepair.ToLower().Equals("true") ? true : false;
                        row.isSales = obj.doortype.issales.ToLower().Equals("true") ? true : false;
                        row.Notes = obj.doortype.notes;

                        db.SaveChanges();

                        Models.Inputs.DoorType dt = new Models.Inputs.DoorType();
                        dt.iscleaning = row.IsCleaning.ToString();
                        dt.iscollection = row.IsCollection.ToString();
                        dt.iscompleted = row.isCompleted.ToString();
                        dt.iscutting = row.IsCuttingForGlass.ToString();
                        dt.isdoor = row.IsDoor.ToString();
                        dt.isfoom = row.IsFoom.ToString();
                        dt.isframeinside = row.IsFrameInside.ToString();
                        dt.isframeoutside = row.IsFrameOutside.ToString();
                        dt.isglassfitting = row.IsGlassFitting.ToString();
                        dt.ispainting = row.IsPainting.ToString();
                        dt.isrepair = row.isRepair.ToString();
                        dt.issales = row.isSales.ToString();
                        dt.id = row.ID.ToString();
                        dt.notes = row.Notes;
                        dt.woid = row.WOID.ToString();
                        dt.quantity = row.Quantity;

                        resp.doortype = dt;
                    }
                }


            }
            else if (obj.type == "aluminiumtype")
            {
                ReportwoAluminium rptal = new ReportwoAluminium();
                rptal.ActionDone = obj.actiondone;
                rptal.CreatedDate = DateTime.Now;
                rptal.SupervisorID = Convert.ToInt32(obj.supervisorid);
                rptal.TechnicianID = Convert.ToInt32(obj.technicianid);
                rptal.WOID = Convert.ToInt32(obj.woid);
                rptal.WOQuantity = Convert.ToInt32(obj.woquantity);

                db.ReportwoAluminiums.Add(rptal);
                db.SaveChanges();

                if (!string.IsNullOrEmpty(obj.aluminiumtype.id))
                {
                    int id = Convert.ToInt32(obj.aluminiumtype.id);

                    var row = db.WO_Aluminium_Details.Where(x => x.ID == id).FirstOrDefault();
                    if (row != null)
                    {
                        row.isAluminiumCut = obj.aluminiumtype.isaluminiumcut.ToLower().Equals("true") ? true : false;
                        row.isCompleted = obj.aluminiumtype.iscompleted.ToLower().Equals("true") ? true : false;
                        row.isCuts = obj.aluminiumtype.iscuts.ToLower().Equals("true") ? true : false;
                        row.isGathering = obj.aluminiumtype.isgathering.ToLower().Equals("true") ? true : false;
                        row.isGlassCut = obj.aluminiumtype.isglasscut.ToLower().Equals("true") ? true : false;
                        row.isInstallAluminium = obj.aluminiumtype.isinstallaluminium.ToLower().Equals("true") ? true : false;
                        row.isInstallationGlass = obj.aluminiumtype.isinstallationglass.ToLower().Equals("true") ? true : false;
                        row.isInstallationOther = obj.aluminiumtype.isinstallationother.ToLower().Equals("true") ? true : false;
                        row.isInstallGlass = obj.aluminiumtype.isinstallglass.ToLower().Equals("true") ? true : false;
                        row.isInstallOther = obj.aluminiumtype.isinstallother.ToLower().Equals("true") ? true : false;
                        row.isOutSideInstallation = obj.aluminiumtype.isoutsideinstallation.ToLower().Equals("true") ? true : false;
                        row.isRepair = obj.aluminiumtype.isrepair.ToLower().Equals("true") ? true : false;
                        row.isSales = obj.aluminiumtype.issales.ToLower().Equals("true") ? true : false;
                        row.isSendAluminium = obj.aluminiumtype.issendaluminium.ToLower().Equals("true") ? true : false;
                        row.isSendGlass = obj.aluminiumtype.issendglass.ToLower().Equals("true") ? true : false;
                        row.isSendOther = obj.aluminiumtype.issendother.ToLower().Equals("true") ? true : false;
                        row.Notes = obj.aluminiumtype.notes;

                        db.SaveChanges();

                        Models.Inputs.AluminiumType alt = new Models.Inputs.AluminiumType();
                        alt.isaluminiumcut = row.isAluminiumCut.ToString();
                        alt.iscompleted = row.isCompleted.ToString();
                        alt.iscuts = row.isCuts.ToString();
                        alt.isgathering = row.isGathering.ToString();
                        alt.isglasscut = row.isGlassCut.ToString();
                        alt.isinstallaluminium = row.isInstallAluminium.ToString();
                        alt.isinstallationglass = row.isInstallationGlass.ToString();
                        alt.isinstallationother = row.isInstallationOther.ToString();
                        alt.isinstallglass = row.isInstallGlass.ToString();
                        alt.isinstallother = row.isInstallOther.ToString();
                        alt.isoutsideinstallation = row.isOutSideInstallation.ToString();
                        alt.isrepair = row.isRepair.ToString();
                        alt.issales = row.isSales.ToString();
                        alt.issendaluminium = row.isSendAluminium.ToString();
                        alt.issendglass = row.isSendGlass.ToString();
                        alt.issendother = row.isSendOther.ToString();
                        alt.id = row.ID.ToString();
                        alt.notes = row.Notes;
                        alt.woid = row.WOID.ToString();
                        alt.quantity = row.Quantity;

                        resp.aluminiumtype = alt;
                    }
                }
            }
            else if (obj.type == "tanktype")
            {
                ReportwoTank rpttnk = new ReportwoTank();
                rpttnk.ActionDone = obj.actiondone;
                rpttnk.CreatedDate = DateTime.Now;
                rpttnk.SupervisorID = Convert.ToInt32(obj.supervisorid);
                rpttnk.TechnicianID = Convert.ToInt32(obj.technicianid);
                rpttnk.WOID = Convert.ToInt32(obj.woid);
                rpttnk.WOQuantity = Convert.ToInt32(obj.woquantity);

                db.ReportwoTanks.Add(rpttnk);
                db.SaveChanges();

                if (!string.IsNullOrEmpty(obj.tanktype.id))
                {
                    int id = Convert.ToInt32(obj.tanktype.id);

                    var row = db.WO_Tanks_Details.Where(x => x.ID == id).FirstOrDefault();
                    if (row != null)
                    {
                        row.isCompleted = obj.tanktype.iscompleted.ToLower().Equals("true") ? true : false;
                        row.isDeliveryInFactory = obj.tanktype.isdeiveryinfactory.ToLower().Equals("true") ? true : false;
                        row.isDeliveryInGround = obj.tanktype.isdeliveryinground.ToLower().Equals("true") ? true : false;
                        row.isDeliveryInHouse = obj.tanktype.isdeliveryinhouse.ToLower().Equals("true") ? true : false;
                        row.isDoProcess = obj.tanktype.isdoprocess.ToLower().Equals("true") ? true : false;
                        row.isExport = obj.tanktype.isexport.ToLower().Equals("true") ? true : false;
                        row.isHeatBarrel = obj.tanktype.isheatbarrel.ToLower().Equals("true") ? true : false;
                        row.isOutput = obj.tanktype.isoutput.ToLower().Equals("true") ? true : false;
                        row.isRepair = obj.tanktype.isrepair.ToLower().Equals("true") ? true : false;
                        row.isSales = obj.tanktype.issales.ToLower().Equals("true") ? true : false;
                        row.isStartMachine = obj.tanktype.isstartmachine.ToLower().Equals("true") ? true : false;
                        row.Notes = obj.tanktype.notes;
                        db.SaveChanges();

                        Models.Inputs.TankType tt = new Models.Inputs.TankType();
                        tt.iscompleted = row.isCompleted.ToString();
                        tt.isdeiveryinfactory = row.isDeliveryInFactory.ToString();
                        tt.isdeliveryinground = row.isDeliveryInGround.ToString();
                        tt.isdeliveryinhouse = row.isDeliveryInHouse.ToString();
                        tt.isdoprocess = row.isDoProcess.ToString();
                        tt.isexport = row.isExport.ToString();
                        tt.isheatbarrel = row.isHeatBarrel.ToString();
                        tt.isoutput = row.isOutput.ToString();
                        tt.isrepair = row.isRepair.ToString();
                        tt.issales = row.isSales.ToString();
                        tt.isstartmachine = row.isStartMachine.ToString();
                        tt.notes = row.Notes;
                        tt.id = row.ID.ToString();
                        tt.notes = row.Notes;
                        tt.woid = row.WOID.ToString();
                        tt.quantity = row.Quantity;

                        resp.tanktype = tt;
                    }
                }
            }
            else if (obj.type == "grpsectiontype")
            {
                ReportwoGRPSection rptgrp = new ReportwoGRPSection();
                rptgrp.ActionDone = obj.actiondone;
                rptgrp.CreatedDate = DateTime.Now;
                rptgrp.SupervisorID = Convert.ToInt32(obj.supervisorid);
                rptgrp.TechnicianID = Convert.ToInt32(obj.technicianid);
                rptgrp.WOID = Convert.ToInt32(obj.woid);
                rptgrp.WOQuantity = Convert.ToInt32(obj.woquantity);

                db.ReportwoGRPSections.Add(rptgrp);
                db.SaveChanges();

                if (!string.IsNullOrEmpty(obj.grpsectiontype.id))
                {
                    int id = Convert.ToInt32(obj.grpsectiontype.id);

                    var row = db.WO_GRPSection_Details.Where(x => x.ID == id).FirstOrDefault();
                    if (row != null)
                    {
                        row.isCheckLeakage = obj.grpsectiontype.ischeckleakage.ToLower().Equals("true") ? true : false;
                        row.isCheckOil = obj.grpsectiontype.ischeckoil.ToLower().Equals("true") ? true : false;
                        row.isCompleteAssembleonSite = obj.grpsectiontype.iscompleteassembleonsite.ToLower().Equals("true") ? true : false;
                        row.isCompleted = obj.grpsectiontype.iscompleted.ToLower().Equals("true") ? true : false;
                        row.isDeliveryAccessories = obj.grpsectiontype.isdeliveryaccessories.ToLower().Equals("true") ? true : false;
                        row.isDeliveryPanelinFactory = obj.grpsectiontype.isdeliverypanelinfact.ToLower().Equals("true") ? true : false;
                        row.isDeliveryPanelinSite = obj.grpsectiontype.isdeliverypanelinsite.ToLower().Equals("true") ? true : false;
                        row.isMixingofLMPs = obj.grpsectiontype.ismixingoflmps.ToLower().Equals("true") ? true : false;
                        row.isMixingofWhitePigment = obj.grpsectiontype.ismixingofwhitepigment.ToLower().Equals("true") ? true : false;
                        row.isRepair = obj.grpsectiontype.isrepair.ToLower().Equals("true") ? true : false;
                        row.isSales = obj.grpsectiontype.issales.ToLower().Equals("true") ? true : false;
                        row.isStartboiler = obj.grpsectiontype.isstartboiler.ToLower().Equals("true") ? true : false;
                        row.isStartCiculation = obj.grpsectiontype.isstartcirculation.ToLower().Equals("true") ? true : false;
                        row.isStartCompound = obj.grpsectiontype.isstartcompound.ToLower().Equals("true") ? true : false;
                        row.isStartMachine = obj.grpsectiontype.isstartmachine.ToLower().Equals("true") ? true : false;
                        row.isStartSheet = obj.grpsectiontype.isstartsheet.ToLower().Equals("true") ? true : false;
                        row.isSteel = obj.grpsectiontype.issteel.ToLower().Equals("true") ? true : false;
                        row.isWaitTemperature = obj.grpsectiontype.iswaittemp.ToLower().Equals("true") ? true : false;
                        row.Notes = obj.grpsectiontype.notes;
                        db.SaveChanges();

                        Models.Inputs.GRPSectionType grpt = new Models.Inputs.GRPSectionType();
                        grpt.iscompleted = row.isCompleted.ToString();
                        grpt.ischeckleakage = row.isCheckLeakage.ToString();
                        grpt.ischeckoil = row.isCheckOil.ToString();
                        grpt.iscompleteassembleonsite = row.isCompleteAssembleonSite.ToString();
                        grpt.isdeliveryaccessories = row.isDeliveryAccessories.ToString();
                        grpt.isdeliverypanelinfact = row.isDeliveryPanelinFactory.ToString();
                        grpt.isdeliverypanelinsite = row.isDeliveryPanelinSite.ToString();
                        grpt.ismixingoflmps = row.isMixingofLMPs.ToString();
                        grpt.ismixingofwhitepigment = row.isMixingofWhitePigment.ToString();
                        grpt.isrepair = row.isRepair.ToString();
                        grpt.issales = row.isSales.ToString();
                        grpt.isstartboiler = row.isStartboiler.ToString();
                        grpt.isstartcirculation = row.isStartCiculation.ToString();
                        grpt.isstartcompound = row.isStartCompound.ToString();
                        grpt.isstartmachine = row.isStartMachine.ToString();
                        grpt.isstartsheet = row.isStartSheet.ToString();
                        grpt.issteel = row.isSteel.ToString();
                        grpt.iswaittemp = row.isWaitTemperature.ToString();
                        grpt.id = row.ID.ToString();
                        grpt.notes = row.Notes;
                        grpt.woid = row.WOID.ToString();
                        grpt.quantity = row.Quantity;

                        resp.grpsectiontype = grpt;
                    }
                }
            }
            else if (obj.type == "glassfybretype")
            {
                ReportwoGFHandLayer rptgf = new ReportwoGFHandLayer();
                rptgf.ActionDone = obj.actiondone;
                rptgf.CreatedDate = DateTime.Now;
                rptgf.SupervisorID = Convert.ToInt32(obj.supervisorid);
                rptgf.TechnicianID = Convert.ToInt32(obj.technicianid);
                rptgf.WOID = Convert.ToInt32(obj.woid);
                rptgf.WOQuantity = Convert.ToInt32(obj.woquantity);

                db.ReportwoGFHandLayers.Add(rptgf);
                db.SaveChanges();

                if (!string.IsNullOrEmpty(obj.glassfybretype.id))
                {
                    int id = Convert.ToInt32(obj.glassfybretype.id);

                    var row = db.WO_GFHandLayer_Details.Where(x => x.ID == id).FirstOrDefault();
                    if (row != null)
                    {
                        row.isCompleted = obj.glassfybretype.iscompleted.ToLower().Equals("true") ? true : false;
                        row.isFixing = obj.glassfybretype.isfixing.ToLower().Equals("true") ? true : false;
                        row.isHandLayUp = obj.glassfybretype.ishandlayup.ToLower().Equals("true") ? true : false;
                        row.isRepair = obj.glassfybretype.isrepair.ToLower().Equals("true") ? true : false;
                        row.isSales = obj.glassfybretype.issales.ToLower().Equals("true") ? true : false;
                        row.isSupplyToFactory = obj.glassfybretype.issupplytofactory.ToLower().Equals("true") ? true : false;
                        row.isSupplyToSite = obj.glassfybretype.issupplytosite.ToLower().Equals("true") ? true : false;
                        row.Notes = obj.glassfybretype.notes;

                        db.SaveChanges();

                        Models.Inputs.GFHLType gft = new Models.Inputs.GFHLType();
                        gft.iscompleted = row.isCompleted.ToString();
                        gft.isfixing = row.isFixing.ToString();
                        gft.ishandlayup = row.isHandLayUp.ToString();
                        gft.isrepair = row.isRepair.ToString();
                        gft.issales = row.isSales.ToString();
                        gft.issupplytofactory = row.isSupplyToFactory.ToString();
                        gft.issupplytosite = row.isSupplyToSite.ToString();
                        gft.id = row.ID.ToString();
                        gft.notes = row.Notes;
                        gft.woid = row.WOID.ToString();
                        gft.quantity = row.Quantity;
                        resp.glassfybretype = gft;
                    }
                }
            }

            return resp;
        }
        private Models.Outputs.GetTechniciansDTO gettechnicians()
        {
            Models.Outputs.GetTechniciansDTO resp = new GetTechniciansDTO();

            List<Technicians> techlst = new List<Technicians>();

            var rows = db.TabletUsers.Where(x => x.isDeleted == false && x.ID == 2 && x.isActive == true).ToList();
            foreach (var row in rows)
            {
                Technicians tech = new Technicians();
                tech.id = row.ID.ToString();
                tech.mobile = row.Mobile;
                tech.role = gettabletrolebyid(Convert.ToInt32(row.TabletRoleID));
                tech.techname = row.Name;
                tech.username = row.Username;

                techlst.Add(tech);
            }
            resp.technicians = techlst;
            return resp;
        }
        private Models.Outputs.GetRolesDTO getroles()
        {
            Models.Outputs.GetRolesDTO resp = new GetRolesDTO();

            List<RoleDTO> rllst = new List<RoleDTO>();
            var rows = db.TabletRoles.ToList();

            foreach (var row in rows)
            {
                RoleDTO rl = new RoleDTO();

                rl.roleid = row.ID.ToString();
                rl.rolename = row.Name;
                rllst.Add(rl);
            }
            resp.roles = rllst;
            return resp;
        }
        private bool isvalidlogin(string username, string password)
        {
            var row = db.TabletUsers.Where(x => x.Username == username && x.Password == password
            && x.isActive == true && x.isDeleted == false).FirstOrDefault();
            if (row == null)
            {
                return false;
            }
            tu = row;
            return true;
        }
        private RoleDTO gettabletrolebyid(int roleid)
        {
            RoleDTO resp = new RoleDTO();

            var row = db.TabletRoles.Where(x => x.ID == roleid && x.Deleted == false).FirstOrDefault();
            if (row != null)
            {
                resp.roleid = row.ID.ToString();
                resp.rolename = row.Name;
            }
            return resp;
        }
        private GetWOBySupervisorDTO getwobysupervisorid(int supervisorid)
        {
            GetWOBySupervisorDTO resp = new GetWOBySupervisorDTO();

            DoorTypeDTO drtypedto = new DoorTypeDTO();
            AluminiumTypeDTO altypedto = new AluminiumTypeDTO();
            TankTypeDTO tnktypedto = new TankTypeDTO();
            GRPSectionTypeDTO grpsectiontypedto = new GRPSectionTypeDTO();
            GlassFybreTypeDTO gftypedto = new GlassFybreTypeDTO();

            drtypedto.wos = getwodoorsteps(supervisorid);
            altypedto.wos = getwoaluminiumsteps(supervisorid);
            tnktypedto.wos = getwotanksteps(supervisorid);
            grpsectiontypedto.wos = geteogrpsectionsteps(supervisorid);
            gftypedto.wos = getwogfhlsteps(supervisorid);

            resp.doortype = drtypedto;
            resp.aluminiumtype = altypedto;
            resp.tanktype = tnktypedto;
            resp.grpsectiontype = grpsectiontypedto;
            resp.glassfybretype = gftypedto;

            return resp;

        }
        private List<WODoortype> getwodoortype(int woid)
        {
            List<WODoortype> wodrtypelst = new List<WODoortype>();

            var drrws = db.WO_Door_Details.Where(x => x.WOID == woid
                            && x.isDeleted == false).ToList();
            foreach (var drrw in drrws)
            {
                WODoortype wodr = new WODoortype();
                wodr.color = drrw.Color;
                wodr.doortypeid = drrw.DoorTypeID.ToString();
                wodr.height = drrw.Height;
                wodr.hours = drrw.Hours;
                wodr.id = drrw.ID.ToString();
                wodr.woid = drrw.WOID.ToString();
                wodr.iscleaning = drrw.IsCleaning.ToString();
                wodr.iscollection = drrw.IsCollection.ToString();
                wodr.iscutting = drrw.IsCuttingForGlass.ToString();
                wodr.isdoor = drrw.IsDoor.ToString();
                wodr.isfoom = drrw.IsFoom.ToString();
                wodr.isframeinside = drrw.IsFrameInside.ToString();
                wodr.isframeoutside = drrw.IsFrameOutside.ToString();
                wodr.isglassfitting = drrw.IsGlassFitting.ToString();
                wodr.ispainting = drrw.IsPainting.ToString();
                wodr.isrepair = drrw.isRepair.ToString();
                wodr.issales = drrw.isSales.ToString();
                wodr.notes = drrw.Notes;
                wodr.quantity = drrw.Quantity;
                wodr.serialno = drrw.SerialNumber;
                wodr.wallwidth = drrw.WallWidth;
                wodr.width = drrw.Width;
                wodr.iscompleted = drrw.isCompleted.ToString();
                wodrtypelst.Add(wodr);
            }

            return wodrtypelst;
        }
        private List<wodoor> getwodoorsteps(int supervisorid)
        {
            List<wodoor> wdlst = new List<wodoor>();

            var wdrws = db.WO_Door.Where(x => x.SupervisorID == supervisorid && x.isDeleted == false && x.isCompleted == false).ToList();

            foreach (var wdrw in wdrws)
            {
                wodoor wd = new wodoor();
                wodoorstep wdstp = new wodoorstep();

                wdstp.color = wdrw.Color;
                if (wdrw.DoorTypeID != null)
                {
                    wdstp.doortype = getdoortypebyid(Convert.ToInt32(wdrw.DoorTypeID));
                }
                wdstp.height = wdrw.Height;
                wdstp.hours = wdrw.Hours;
                wdstp.id = wdrw.ID.ToString();
                wdstp.iscleaning = wdrw.IsCleaning.ToString();
                wdstp.iscollection = wdrw.IsCollection.ToString();
                wdstp.iscutting = wdrw.IsCuttingForGlass.ToString();
                wdstp.isdoor = wdrw.IsDoor.ToString();
                wdstp.isfoom = wdrw.IsFoom.ToString();
                wdstp.isframeinside = wdrw.IsFrameInside.ToString();
                wdstp.isframeoutside = wdrw.IsFrameOutside.ToString();
                wdstp.isglassfitting = wdrw.IsGlassFitting.ToString();
                wdstp.ispainting = wdrw.IsPainting.ToString();
                wdstp.isrepair = wdrw.isRepair.ToString();
                wdstp.issales = wdrw.isSales.ToString();
                wdstp.iscompleted = wdrw.isCompleted.ToString();
                wdstp.notes = wdrw.Notes;
                wdstp.quantity = wdrw.Quantity;
                wdstp.serialno = wdrw.SerialNumber;
                wdstp.wallwidth = wdrw.WallWidth;
                wdstp.width = wdrw.Width;

                wd.wodoorsteps = wdstp;
                wd.wodoortypes = getwodoortype(wdrw.ID);

                wdlst.Add(wd);
            }

            return wdlst;
        }
        private List<WOAluminiumType> getwoaluminiumtype(int woid)
        {
            List<WOAluminiumType> woaltyplst = new List<WOAluminiumType>();

            var alrws = db.WO_Aluminium_Details.Where(x => x.WOID == woid
             && x.isDeleted == false).ToList();

            foreach (var alrw in alrws)
            {
                WOAluminiumType woaltype = new WOAluminiumType();
                woaltype.aluminiumtypeid = alrw.AluminiumTypeID.ToString();
                woaltype.color = alrw.Color;
                woaltype.height = alrw.Height;
                woaltype.hours = alrw.Hours;
                woaltype.id = alrw.ID.ToString();
                woaltype.isaluminiumcut = alrw.isAluminiumCut.ToString();
                woaltype.iscuts = alrw.isCuts.ToString();
                woaltype.isgathering = alrw.isGathering.ToString();
                woaltype.isglasscut = alrw.isGlassCut.ToString();
                woaltype.isinstallaluminium = alrw.isInstallAluminium.ToString();
                woaltype.isinstallationglass = alrw.isInstallationGlass.ToString();
                woaltype.isinstallationother = alrw.isInstallationOther.ToString();
                woaltype.isinstallglass = alrw.isInstallGlass.ToString();
                woaltype.isinstallother = alrw.isInstallOther.ToString();
                woaltype.isoutsideinstallation = alrw.isOutSideInstallation.ToString();
                woaltype.isrepair = alrw.isRepair.ToString();
                woaltype.issales = alrw.isSales.ToString();
                woaltype.issendaluminium = alrw.isSendAluminium.ToString();
                woaltype.issendglass = alrw.isSendGlass.ToString();
                woaltype.issendother = alrw.isSendOther.ToString();
                woaltype.iscompleted = alrw.isCompleted.ToString();
                woaltype.notes = alrw.Notes;
                woaltype.quantity = alrw.Quantity;
                woaltype.thickness = alrw.Thickness;
                woaltype.width = alrw.Width;
                woaltype.woid = alrw.WOID.ToString();
                woaltyplst.Add(woaltype);
            }
            return woaltyplst;
        }
        private List<woaluminium> getwoaluminiumsteps(int supervisorid)
        {
            List<woaluminium> walst = new List<woaluminium>();

            var rows = db.WO_Aluminium.Where(x => x.SupervisorID == supervisorid && x.isDeleted == false && x.isCompleted == false).ToList();

            foreach (var row in rows)
            {
                woaluminium woa = new woaluminium();

                woaluminiumstep woalstp = new woaluminiumstep();
                if (row.AluminiumTypeID != null)
                {
                    woalstp.aluminiumtype = getaluminiumtypebyid(Convert.ToInt32(row.AluminiumTypeID));
                }
                woalstp.color = row.Color;
                woalstp.height = row.Height;
                woalstp.hours = row.Hours;
                woalstp.id = row.ID.ToString();
                woalstp.isaluminiumcut = row.isAluminiumCut.ToString();
                woalstp.iscuts = row.isCuts.ToString();
                woalstp.isgathering = row.isGathering.ToString();
                woalstp.isglasscut = row.isGlassCut.ToString();
                woalstp.isinstallaluminium = row.isInstallAluminium.ToString();
                woalstp.isinstallationglass = row.isInstallationGlass.ToString();
                woalstp.isinstallationother = row.isInstallationOther.ToString();
                woalstp.isinstallglass = row.isInstallGlass.ToString();
                woalstp.isinstallother = row.isInstallOther.ToString();
                woalstp.isoutsideinstallation = row.isOutSideInstallation.ToString();
                woalstp.isrepair = row.isRepair.ToString();
                woalstp.issales = row.isSales.ToString();
                woalstp.issendaluminium = row.isSendAluminium.ToString();
                woalstp.issendglass = row.isSendGlass.ToString();
                woalstp.issendother = row.isSendOther.ToString();
                woalstp.notes = row.Notes;
                woalstp.quantity = row.Quantity;
                woalstp.thickness = row.Thickness;
                woalstp.width = row.Width;
                woalstp.iscompleted = row.isCompleted.ToString();

                woa.woaluminiumstpes = woalstp;
                woa.woaluminiumtypes = getwoaluminiumtype(row.ID);

                walst.Add(woa);
            }

            return walst;
        }
        private List<WOTankType> getwotanktype(int woid)
        {
            List<WOTankType> wottlst = new List<WOTankType>();

            var rows = db.WO_Tanks_Details.Where(x => x.WOID == woid && x.isDeleted == false).ToList();

            foreach (var row in rows)
            {
                WOTankType wott = new WOTankType();
                wott.gallons = row.Gallons;
                wott.hours = row.Hours;
                wott.id = row.ID.ToString();
                wott.iscompleted = row.isCompleted.ToString();
                wott.isdeiveryinfactory = row.isDeliveryInFactory.ToString();
                wott.isdeliveryinground = row.isDeliveryInGround.ToString();
                wott.isdeliveryinhouse = row.isDeliveryInHouse.ToString();
                wott.isdoprocess = row.isDoProcess.ToString();
                wott.isexport = row.isExport.ToString();
                wott.isheatbarrel = row.isHeatBarrel.ToString();
                wott.isoutput = row.isOutput.ToString();
                wott.isrepair = row.isRepair.ToString();
                wott.issales = row.isSales.ToString();
                wott.isstartmachine = row.isStartMachine.ToString();
                wott.layer = row.Layers;
                wott.machineno = row.MachineNumber;
                wott.notes = row.Notes;
                wott.productname = row.ProductName;
                wott.qauntity = row.Quantity;
                wott.tanktypeid = row.TankTypeID.ToString();
                wott.woid = row.isCompleted.ToString();
                wottlst.Add(wott);
            }

            return wottlst;
        }
        private List<wotank> getwotanksteps(int supervisorid)
        {
            List<wotank> wotlst = new List<wotank>();

            var rows = db.WO_Tanks.Where(x => x.SupervisorID == supervisorid && x.isDeleted == false && x.isCompleted == false).ToList();

            foreach (var row in rows)
            {
                wotank wot = new wotank();
                wotankstep wotstp = new wotankstep();

                wotstp.gallons = row.Gallons;
                wotstp.hours = row.Hours;
                wotstp.id = row.ID.ToString();
                wotstp.isdeiveryinfactory = row.isDeliveryInFactory.ToString();
                wotstp.isdeliveryinground = row.isDeliveryInGround.ToString();
                wotstp.isdeliveryinhouse = row.isDeliveryInHouse.ToString();
                wotstp.isdoprocess = row.isDoProcess.ToString();
                wotstp.isexport = row.isExport.ToString();
                wotstp.isheatbarrel = row.isHeatBarrel.ToString();
                wotstp.isoutput = row.isOutput.ToString();
                wotstp.isrepair = row.isRepair.ToString();
                wotstp.issales = row.isSales.ToString();
                wotstp.isstartmachine = row.isStartMachine.ToString();
                wotstp.layer = row.Layers;
                wotstp.machineno = row.MachineNumber;
                wotstp.notes = row.Notes;
                wotstp.productname = row.ProductName;
                wotstp.qauntity = row.Quantity;
                wotstp.iscompleted = row.isCompleted.ToString();
                if (row.TankTypeID != null)
                {
                    wotstp.tanktype = gettanktypebyid(Convert.ToInt32(row.TankTypeID));
                }
                wot.wotanksteps = wotstp;
                wot.wotanktypes = getwotanktype(row.ID);

                wotlst.Add(wot);
            }
            return wotlst;
        }
        private List<wogrpsection> geteogrpsectionsteps(int supervisorid)
        {
            List<wogrpsection> resp = new List<wogrpsection>();

            var rows = db.WO_GRPSection.Where(x => x.SupervisorID == supervisorid
              && x.isDeleted == false && x.isCompleted == false).ToList();

            foreach (var row in rows)
            {
                wogrpsection wogrp = new wogrpsection();
                wogrpsectionstep wogrpstp = new wogrpsectionstep();

                if (row.GRPSectionTypeID != null)
                {
                    wogrpstp.grpsectiontype = getgrpsectiontypebyid(Convert.ToInt32(row.GRPSectionTypeID));
                }
                wogrpstp.hours = row.Hours;
                wogrpstp.id = row.ID.ToString();
                wogrpstp.ischeckleakage = row.isCheckLeakage.ToString();
                wogrpstp.ischeckoil = row.isCheckOil.ToString();
                wogrpstp.iscompleteassembleonsite = row.isCompleteAssembleonSite.ToString();
                wogrpstp.isdeliveryaccessories = row.isDeliveryAccessories.ToString();
                wogrpstp.isdeliverypanelinfact = row.isDeliveryPanelinFactory.ToString();
                wogrpstp.isdeliverypanelinsite = row.isDeliveryPanelinSite.ToString();
                wogrpstp.ismixingoflmps = row.isMixingofLMPs.ToString();
                wogrpstp.ismixingofwhitepigment = row.isMixingofWhitePigment.ToString();
                wogrpstp.isrepair = row.isRepair.ToString();
                wogrpstp.issales = row.isSales.ToString();
                wogrpstp.isstartboiler = row.isStartboiler.ToString();
                wogrpstp.isstartcirculation = row.isStartCiculation.ToString();
                wogrpstp.isstartcompound = row.isStartCompound.ToString();
                wogrpstp.isstartmachine = row.isStartMachine.ToString();
                wogrpstp.isstartsheet = row.isStartSheet.ToString();
                wogrpstp.issteel = row.isSteel.ToString();
                wogrpstp.iswaittemp = row.isWaitTemperature.ToString();
                wogrpstp.machineno = row.MachineNumber;
                wogrpstp.notes = row.Notes;
                wogrpstp.processstationid = row.ProcessStationID.ToString();
                wogrpstp.productname = row.ProductName;
                wogrpstp.quantity = row.Quantity;
                wogrpstp.iscompleted = row.isCompleted.ToString();

                wogrp.wogrpsesctionsteps = wogrpstp;
                wogrp.wogrpsectiontype = getwogrpsectiontype(Convert.ToInt32(row.ID));
                resp.Add(wogrp);
            }
            return resp;
        }
        private List<WOGRPSectionType> getwogrpsectiontype(int woid)
        {
            List<WOGRPSectionType> wogrptypelst = new List<WOGRPSectionType>();

            var rows = db.WO_GRPSection_Details.Where(x => x.WOID == woid
              && x.isDeleted == false).ToList();

            foreach (var row in rows)
            {
                WOGRPSectionType wogrptype = new WOGRPSectionType();

                wogrptype.grpsectiontypeid = row.GRPSectionTypeID.ToString();
                wogrptype.hours = row.Hours;
                wogrptype.id = row.ID.ToString();
                wogrptype.woid = row.WOID.ToString();
                wogrptype.ischeckleakage = row.isCheckLeakage.ToString();
                wogrptype.ischeckoil = row.isCheckOil.ToString();
                wogrptype.iscompleteassembleonsite = row.isCompleteAssembleonSite.ToString();
                wogrptype.isdeliveryaccessories = row.isDeliveryAccessories.ToString();
                wogrptype.isdeliverypanelinfact = row.isDeliveryPanelinFactory.ToString();
                wogrptype.isdeliverypanelinsite = row.isDeliveryPanelinSite.ToString();
                wogrptype.ismixingoflmps = row.isMixingofLMPs.ToString();
                wogrptype.ismixingofwhitepigment = row.isMixingofWhitePigment.ToString();
                wogrptype.isrepair = row.isRepair.ToString();
                wogrptype.issales = row.isSales.ToString();
                wogrptype.isstartboiler = row.isStartboiler.ToString();
                wogrptype.isstartcirculation = row.isStartCiculation.ToString();
                wogrptype.isstartcompound = row.isStartCompound.ToString();
                wogrptype.isstartmachine = row.isStartMachine.ToString();
                wogrptype.isstartsheet = row.isStartSheet.ToString();
                wogrptype.issteel = row.isSteel.ToString();
                wogrptype.iswaittemp = row.isWaitTemperature.ToString();
                wogrptype.machineno = row.MachineNumber;
                wogrptype.notes = row.Notes;
                wogrptype.processstationid = row.ProcessStationID.ToString();
                wogrptype.productname = row.ProductName;
                wogrptype.quantity = row.Quantity;

                wogrptypelst.Add(wogrptype);
            }


            return wogrptypelst;
        }
        private List<woglassfybre> getwogfhlsteps(int supervisorid)
        {
            List<woglassfybre> wogflst = new List<woglassfybre>();
            var rows = db.WO_GFHandLayer.Where(x => x.SupervisorID == supervisorid
              && x.isDeleted == false && x.isCompleted == false).ToList();

            foreach (var row in rows)
            {
                woglassfybre wogf = new woglassfybre();
                wogfhlstep wogfstp = new wogfhlstep();

                wogfstp.gfhltype = getgfhltypebyid(Convert.ToInt32(row.GFHLTypeID));
                wogfstp.hours = row.Hours;
                wogfstp.id = row.ID.ToString();
                wogfstp.isfixing = row.isFixing.ToString();
                wogfstp.ishandlayup = row.isHandLayUp.ToString();
                wogfstp.isrepair = row.isRepair.ToString();
                wogfstp.issales = row.isSales.ToString();
                wogfstp.issupplytofactory = row.isSupplyToFactory.ToString();
                wogfstp.issupplytosite = row.isSupplyToSite.ToString();
                wogfstp.machineno = row.MachineNumber;
                wogfstp.notes = row.Notes;
                wogfstp.processstationid = row.ProcessStationID.ToString();
                wogfstp.prodcutname = row.ProductName;
                wogfstp.quantity = row.Quantity;
                wogfstp.iscompleted = row.isCompleted.ToString();

                wogf.wogfhlsteps = wogfstp;
                wogf.wogfhltypes = getwogfhltype(row.ID);
                wogflst.Add(wogf);
            }

            return wogflst;
        }
        private List<WOGFHLType> getwogfhltype(int woid)
        {
            List<WOGFHLType> wogflst = new List<WOGFHLType>();

            var rows = db.WO_GFHandLayer_Details.Where(x => x.WOID == woid
               && x.isDeleted == false).ToList();

            foreach (var row in rows)
            {
                WOGFHLType wogf = new WOGFHLType();

                wogf.gfhltypeid = row.GFHLTypeID.ToString();
                wogf.hours = row.Hours;
                wogf.id = row.ID.ToString();
                wogf.isfixing = row.isFixing.ToString();
                wogf.ishandlayup = row.isHandLayUp.ToString();
                wogf.isrepair = row.isRepair.ToString();
                wogf.issales = row.isSales.ToString();
                wogf.issupplytofactory = row.isSupplyToFactory.ToString();
                wogf.issupplytosite = row.isSupplyToSite.ToString();
                wogf.machineno = row.MachineNumber;
                wogf.notes = row.Notes;
                wogf.processstationid = row.ProcessStationID.ToString();
                wogf.prodcutname = row.ProductName;
                wogf.iscompleted = row.isCompleted.ToString();
                wogf.quantity = row.Quantity;
                wogf.woid = row.WOID.ToString();

                wogflst.Add(wogf);
            }
            return wogflst;
        }

        #region GetTypesbyid

        private GetDoorType getdoortypebyid(int id)
        {
            Models.Outputs.GetDoorType resp = new Models.Outputs.GetDoorType();

            var rows = db.sp_Get_DoorTypes(id);
            foreach (var row in rows)
            {
                resp.doortypename = row.DoorTypeName;
                resp.id = row.ID.ToString();
                resp.picture = row.Picture;
                resp.serialno = row.SerialNumber;
            }

            return resp;
        }
        private GetAluminiumType getaluminiumtypebyid(int id)
        {
            GetAluminiumType resp = new GetAluminiumType();

            var rows = db.sp_Get_AluminiumTypes(id);
            foreach (var row in rows)
            {
                resp.aluminiumtypename = row.AluminiumTypeName;
                resp.id = row.ID.ToString();
                resp.picture = row.Picture;
                resp.serialno = row.SerialNumber;
            }

            return resp;
        }
        private GetTankType gettanktypebyid(int id)
        {
            GetTankType resp = new GetTankType();

            var rows = db.sp_Get_TankTypes(id, "");

            foreach (var row in rows)
            {
                resp.gallon = row.Gallon;
                resp.machineno = row.MachineNumber;
                resp.id = row.ID.ToString();
                resp.processstationid = row.ProcessStationID.ToString();
                resp.productname = row.ProductName;
                resp.picture = row.Picture;
                resp.processstationame = row.ProcessStationName;
            }
            return resp;
        }
        private GetGRPSectionType getgrpsectiontypebyid(int id)
        {
            GetGRPSectionType resp = new GetGRPSectionType();
            var rows = db.sp_Get_GRPSectionTypes(id, "");

            foreach (var row in rows)
            {
                resp.machineno = row.MachineNumber;
                resp.id = row.ID.ToString();
                resp.processstationid = row.ProcessStationID.ToString();
                resp.productname = row.ProductName;
                resp.picture = row.Picture;
                resp.processstationname = row.ProcessStationName;
            }
            return resp;
        }
        private GetGFHLType getgfhltypebyid(int id)
        {
            GetGFHLType resp = new GetGFHLType();

            var rows = db.sp_Get_GlassFybreTypes(id, "");

            foreach (var row in rows)
            {
                resp.id = row.ID.ToString();
                resp.machineno = row.MachineNumber;
                resp.picture = row.Picture;
                resp.processstationid = row.ProcessStationID.ToString();
                resp.processstationname = row.ProcessStationName;
                resp.productname = row.ProductName;
            }
            return resp;
        }

        #endregion

        #endregion

    }
}
