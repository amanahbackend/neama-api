﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models
{
    public class Status
    {
        public string statuscode { get; set; }
        public string statusdescription { get; set; }
        public string info1 { get; set; }
        public string info2 { get; set; }
        //public Translations translations { get; set; }

        public Status()
        {

        }

        public Status(int i)
        {
            switch (i)
            {
                case 0:
                    {
                        statuscode = "0";
                        statusdescription = "SUCCESS";
                        break;
                    }
                case 1:
                    {
                        statuscode = "1";
                        statusdescription = "FAILED";
                        break;
                    }

                case 404:
                    {
                        statuscode = "404";
                        statusdescription = "UNAUTHORIZED";
                        break;
                    }

                case 800:
                    {
                        statuscode = "800";
                        statusdescription = "REQUIRED PARAMETERS MISSING/EMPTY";
                        break;
                    }

                case 1040:
                    {
                        statuscode = "1040";
                        statusdescription = "INVALID USER/LOGIN";
                        break;
                    }

                default:
                    {
                        statuscode = "";
                        statusdescription = "";
                        break;
                    }
            }




        }



    }
}
