﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class UploadImages
    {
        public string img { get; set; }
    }
}
