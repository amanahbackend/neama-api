﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class SaveReportTechnician
    {
        public string type { get; set; }
        public string woid { get; set; }
        public string woquantity { get; set; }
        public string supervisorid { get; set; }
        public string technicianid { get; set; }
        public string actiondone { get; set; }
        public DoorType doortype { get; set; }
        public AluminiumType aluminiumtype { get; set; }
        public TankType tanktype { get; set; }
        public GRPSectionType grpsectiontype { get; set; }
        public GFHLType glassfybretype { get; set; }
    }
}
