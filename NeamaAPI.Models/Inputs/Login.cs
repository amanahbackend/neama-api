﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class Login
    {
        //public string roleid { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
