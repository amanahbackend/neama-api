﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class CloseWO
    {
        public string typeid { get; set; }
        public string woid { get; set; }
    }
}
