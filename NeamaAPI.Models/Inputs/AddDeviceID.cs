﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class AddDeviceID
    {
        public string userid { get; set; }
        public string deviceid { get; set; }
    }
}
