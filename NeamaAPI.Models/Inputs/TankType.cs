﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class TankType
    {
        public string id { get; set; }
        public string woid { get; set; }
        public string quantity { get; set; }
        public string isheatbarrel { get; set; }
        public string isstartmachine { get; set; }
        public string isdoprocess { get; set; }
        public string isoutput { get; set; }
        public string isdeliveryinhouse { get; set; }
        public string isdeliveryinground { get; set; }
        public string isdeiveryinfactory { get; set; }
        public string isexport { get; set; }
        public string isrepair { get; set; }
        public string issales { get; set; }
        public string iscompleted { get; set; }
        public string notes { get; set; }
    }
}
