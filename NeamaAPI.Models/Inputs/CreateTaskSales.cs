﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class CreateTaskSales
    {
        public string userid { get; set; }
        public string tasktypeid { get; set; }
        public string datetime { get; set; }
        public string customername { get; set; }
        public string description { get; set; }
        public string remark { get; set; }
        public string taskstatusid { get; set; }
        public List<UploadImages> images { get; set; }
    }
}
