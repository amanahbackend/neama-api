﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class TrackDailyLog
    {
        public string tracktypeid { get; set; }
        public string userid { get; set; }
        public string roleid { get; set; }
        public string latitude { get; set; }
        public string longtitude { get; set; }
    }
}
