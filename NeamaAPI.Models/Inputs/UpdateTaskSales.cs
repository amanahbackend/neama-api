﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class UpdateTaskSales
    {
        public string tasksalesid { get; set; }
        public string taskstatusid { get; set; }
        public string remark { get; set; }
        public List<UploadImages> images { get; set; }
    }
}
