﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class GetSearchTaskMandoop
    {
        public string customername { get; set; }
        public string userid { get; set; }
        public string typeid { get; set; }
        public string taskstatusid { get; set; }
        public string assignfromdate { get; set; }
        public string assigntodate { get; set; }
    }
}
