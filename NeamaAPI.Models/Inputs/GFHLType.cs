﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class GFHLType
    {
        public string id { get; set; }
        public string woid { get; set; }
        public string quantity { get; set; }
        public string ishandlayup { get; set; }
        public string issupplytosite { get; set; }
        public string isfixing { get; set; }
        public string issupplytofactory { get; set; }
        public string isrepair { get; set; }
        public string issales { get; set; }
        public string iscompleted { get; set; }
        public string notes { get; set; }
    }
}
