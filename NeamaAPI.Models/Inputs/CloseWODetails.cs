﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Inputs
{
    public class CloseWODetails
    {
        public string typeid { get; set; }
        public string woid { get; set; }
        public string quantity { get; set; }
    }
}
