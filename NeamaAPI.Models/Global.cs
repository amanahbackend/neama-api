﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models
{
    public class Global
    {
        public static string GetUniqueKey(int KeyLength)
        {
            String a = "123456789";
            Char[] chars = new Char[(a.Length)];
            chars = a.ToCharArray();
            byte[] data = new byte[(KeyLength)];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(KeyLength);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        public static string GetUniqueAlphaNumericKey(int KeyLength)
        {
            String a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789abcdefghijklmnopqrstuvwxyz";
            Char[] chars = new Char[(a.Length)];
            chars = a.ToCharArray();
            byte[] data = new byte[(KeyLength)];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(KeyLength);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
        public static string GetUniqueAlphaKey(int KeyLength)
        {
            String a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            Char[] chars = new Char[(a.Length)];
            chars = a.ToCharArray();
            byte[] data = new byte[(KeyLength)];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(KeyLength);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }


        public static string GetUniqueKeyWithCapitalAlpha(int KeyLength)
        {
            String a = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
            Char[] chars = new Char[(a.Length)];
            chars = a.ToCharArray();
            byte[] data = new byte[(KeyLength)];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(KeyLength);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

    }
}

