﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class GetTaskStatusDTO
    {
        public List<TaskStatus> taskstatus { get; set; }
    }
}
