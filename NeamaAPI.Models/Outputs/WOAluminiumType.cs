﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class WOAluminiumType
    {
        public string id { get; set; }
        public string woid { get; set; }
        public string aluminiumtypeid { get; set; }
        public string quantity { get; set; }
        public string width { get; set; }
        public string height { get; set; }
        public string color { get; set; }
        public string thickness { get; set; }
        public string hours { get; set; }
        public string isaluminiumcut { get; set; }
        public string isglasscut { get; set; }
        public string isgathering { get; set; }
        public string isinstallationglass { get; set; }
        public string isinstallationother { get; set; }
        public string isrepair { get; set; }
        public string issales { get; set; }
        public string iscuts { get; set; }
        public string isoutsideinstallation { get; set; }
        public string issendglass { get; set; }
        public string issendaluminium { get; set; }
        public string issendother { get; set; }
        public string isinstallglass { get; set; }
        public string isinstallaluminium { get; set; }
        public string isinstallother { get; set; }
        public string notes { get; set; }
        public string iscompleted { get; set; }
    }
}
