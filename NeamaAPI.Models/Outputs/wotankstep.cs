﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class wotankstep
    {
        public string id { get; set; }
        public GetTankType tanktype { get; set; }
        public string productname { get; set; }
        public string machineno { get; set; }
        public string gallons { get; set; }
        public string layer { get; set; }
        public string qauntity { get; set; }
        public string hours { get; set; }
        public string isheatbarrel { get; set; }
        public string isstartmachine { get; set; }
        public string isdoprocess { get; set; }
        public string isoutput { get; set; }
        public string isdeliveryinhouse { get; set; }
        public string isdeliveryinground { get; set; }
        public string isdeiveryinfactory { get; set; }
        public string isexport { get; set; }
        public string isrepair { get; set; }
        public string issales { get; set; }
        public string iscompleted { get; set; }
        public string notes { get; set; }
    }
}
