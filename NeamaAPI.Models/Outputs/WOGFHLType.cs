﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class WOGFHLType
    {
        public string id { get; set; }
        public string woid { get; set; }
        public string gfhltypeid { get; set; }
        public string processstationid { get; set; }
        public string prodcutname { get; set; }
        public string machineno { get; set; }
        public string quantity { get; set; }
        public string hours { get; set; }
        public string ishandlayup { get; set; }
        public string issupplytosite { get; set; }
        public string isfixing { get; set; }
        public string issupplytofactory { get; set; }
        public string isrepair { get; set; }
        public string issales { get; set; }
        public string iscompleted { get; set; }
        public string notes { get; set; }
    }
}
