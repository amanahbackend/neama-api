﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
   public class wogrpsectionstep
    {
        public string id { get; set; }
        public GetGRPSectionType grpsectiontype { get; set; }
        public string processstationid { get; set; }
        public string supervisorid { get; set; }
        public string productname { get; set; }
        public string machineno { get; set; }
        public string quantity { get; set; }
        public string hours { get; set; }
        public string ismixingoflmps { get; set; }
        public string ismixingofwhitepigment { get; set; }
        public string isstartcompound { get; set; }
        public string isstartsheet { get; set; }
        public string ischeckoil { get; set; }
        public string isstartcirculation { get; set; }
        public string ischeckleakage { get; set; }
        public string isstartboiler { get; set; }
        public string iswaittemp { get; set; }
        public string isstartmachine { get; set; }
        public string isdeliverypanelinfact { get; set; }
        public string isdeliverypanelinsite { get; set; }
        public string issteel { get; set; }
        public string iscompleteassembleonsite { get; set; }
        public string isdeliveryaccessories { get; set; }
        public string isrepair { get; set; }
        public string issales { get; set; }
        public string iscompleted { get; set; }
        public string notes { get; set; }
    }
}
