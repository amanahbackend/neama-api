﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class wogrpsection
    {
        public wogrpsectionstep wogrpsesctionsteps { get; set; }
        public List<WOGRPSectionType> wogrpsectiontype { get; set; }
    }
}
