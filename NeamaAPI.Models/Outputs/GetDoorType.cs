﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class GetDoorType
    {
        public string id { get; set; }
        public string doortypename { get; set; }
        public string serialno { get; set; }
        public string picture { get; set; }
    }
}
