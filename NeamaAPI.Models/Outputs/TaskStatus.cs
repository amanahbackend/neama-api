﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class TaskStatus
    {
        public string id { get; set; }
        public string taskstatusname { get; set; }
    }
}
