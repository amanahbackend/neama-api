﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class RoleDTO
    {
        public string roleid { get; set; }
        public string rolename { get; set; }
    }
}
