﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class GetGFHLType
    {
        public string id { get; set; }
        public string processstationid { get; set; }
        public string processstationname { get; set; }
        public string productname { get; set; }
        public string machineno { get; set; }
        public string picture { get; set; }
    }
}
