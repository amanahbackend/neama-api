﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class GetTrackTypeDTO
    {
        public List<TrackTypes> tracktypes { get; set; }
    }
}
