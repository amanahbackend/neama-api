﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class CloseWODTO
    {
        public string type { get; set; }
        public string woid { get; set; }
        public string isCompleted { get; set; }
    }
}
