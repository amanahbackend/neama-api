﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class LoginDTO
    {
        public RoleDTO role { get; set; }
        public string userid { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
    }
}
