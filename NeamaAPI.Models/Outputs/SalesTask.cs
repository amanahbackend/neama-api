﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class SalesTask
    {
        public string id { get; set; }
        public TaskTypes tasktype { get; set; }
        public string taskforuserid { get; set; }
        public string datetime { get; set; }
        public string customername { get; set; }
        public string description { get; set; }
        public string remark { get; set; }
        public TaskStatus taskstatus { get; set; }
        public List<Images> images { get; set; }
    }
}
