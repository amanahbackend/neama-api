﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class GetRolesDTO
    {
        public List<RoleDTO> roles { get; set; }
    }
}
