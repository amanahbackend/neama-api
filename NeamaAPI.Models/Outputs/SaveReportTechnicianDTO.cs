﻿using NeamaAPI.Models.Inputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class SaveReportTechnicianDTO
    {
        public DoorType doortype { get; set; }
        public AluminiumType aluminiumtype { get; set; }
        public TankType tanktype { get; set; }
        public GRPSectionType grpsectiontype { get; set; }
        public GFHLType glassfybretype { get; set; }
    }
}
