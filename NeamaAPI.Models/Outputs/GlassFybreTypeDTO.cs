﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class GlassFybreTypeDTO
    {
        public List<woglassfybre> wos { get; set; }
    }
}
