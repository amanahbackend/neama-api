﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class WODoortype
    {
        public string id { get; set; }
        public string woid { get; set; }
        public string serialno { get; set; }
        public string quantity { get; set; }
        public string width { get; set; }
        public string height { get; set; }
        public string color { get; set; }
        public string wallwidth { get; set; }
        public string hours { get; set; }
        public string doortypeid { get; set; }
        public string iscollection { get; set; }
        public string isfoom { get; set; }
        public string iscutting { get; set; }
        public string iscleaning { get; set; }
        public string isglassfitting { get; set; }
        public string ispainting { get; set; }
        public string isframeinside { get; set; }
        public string isframeoutside { get; set; }
        public string isdoor { get; set; }
        public string isrepair { get; set; }
        public string issales { get; set; }
        public string iscompleted { get; set; }
        public string notes { get; set; }
    }
}
