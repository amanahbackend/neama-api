﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class GRPSectionTypeDTO
    {
        public List<wogrpsection> wos { get; set; }
    }
}
