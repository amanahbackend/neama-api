﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class Technicians
    {
        public string id { get; set; }
        public RoleDTO role { get; set; }
        public string techname { get; set; }
        public string username { get; set; }
        public string mobile { get; set; }
    }
}
