﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class wodoor
    {
        public wodoorstep wodoorsteps { get; set; }
        public List<WODoortype> wodoortypes { get; set; }
    }
}
