﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class wotank
    {
        public wotankstep wotanksteps { get; set; }
        public List<WOTankType> wotanktypes { get; set; }
    }
}
