﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeamaAPI.Models.Outputs
{
    public class GetWOBySupervisorDTO
    {
        public DoorTypeDTO doortype { get; set; }
        public AluminiumTypeDTO aluminiumtype { get; set; }
        public TankTypeDTO tanktype { get; set; }
        public GRPSectionTypeDTO grpsectiontype { get; set; }
        public GlassFybreTypeDTO glassfybretype { get; set; }
    }
}
